<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class produk extends Model
{
    protected $fillable = ['id_ub', 'id_kategori', 'nama_produk', 'harga_beli', 'harga_jual', 'keterangan', 'foto'];
}
