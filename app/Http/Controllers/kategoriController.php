<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\kategori;

class kategoriController extends Controller
{
    public function index()
    {
        return kategori::all();
    }

    public function show($id)
    {
        return kategori::find($id);
    }

    public function store(Request $request)
    {
        return kategori::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $kategori = kategori::findOrFail($id);
        $kategori->update($request->all());

        return $kategori;
    }

    public function delete(Request $request, $id)
    {
        $kategori = kategori::findOrFail($id);
        $kategori->delete();

        return 204;
    }
}
