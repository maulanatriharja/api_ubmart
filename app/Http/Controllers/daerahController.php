<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\daerah;

class daerahController extends Controller
{
    public function index()
    {
        return daerah::all();
    }

    public function show($id)
    {
        return daerah::find($id);
    }

    public function store(Request $request)
    {
        return daerah::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $daerah = daerah::findOrFail($id);
        $daerah->update($request->all());

        return $daerah;
    }

    public function delete(Request $request, $id)
    {
        $daerah = daerah::findOrFail($id);
        $daerah->delete();

        return 204;
    }
}
