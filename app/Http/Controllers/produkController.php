<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\produk;

class produkController extends Controller
{
    public function index()
    {
        return produk::all();
    }

    public function show($id)
    {
        return produk::find($id);
    }

    public function store(Request $request)
    {
        return produk::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $produk = produk::findOrFail($id);
        $produk->update($request->all());

        return $produk;
    }

    public function delete(Request $request, $id)
    {
        $produk = produk::findOrFail($id);
        $produk->delete();

        return 204;
    }
}
