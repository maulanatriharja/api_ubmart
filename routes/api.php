<?php

use Illuminate\Http\Request;
use App\daerah;
use App\kategori;
use App\produk;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('daerahs', 'daerahController@index');
Route::get('daerahs/{id}', 'daerahController@show');
Route::post('daerahs', 'daerahController@store');
Route::put('daerahs/{id}', 'daerahController@update');
Route::delete('daerahs/{id}', 'daerahController@delete');

Route::get('kategoris', 'kategoriController@index');
Route::get('kategoris/{id}', 'kategoriController@show');
Route::post('kategoris', 'kategoriController@store');
Route::put('kategoris/{id}', 'kategoriController@update');
Route::delete('kategoris/{id}', 'kategoriController@delete');

Route::get('produks', 'produkController@index');
Route::get('produks/{id}', 'produkController@show');
Route::post('produks', 'produkController@store');
Route::put('produks/{id}', 'produkController@update');
Route::delete('produks/{id}', 'produkController@delete');
