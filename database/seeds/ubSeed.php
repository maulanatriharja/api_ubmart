<?php

use Illuminate\Database\Seeder;
Use App\ub;

class ubSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        ub::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 5; $i++) {
            ub::create([
                'nama_ub' => $faker->streetName                          ,
            ]);
        }
    }
}
