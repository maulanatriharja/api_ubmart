<?php

use Illuminate\Database\Seeder;
Use App\daerah;

class daerahSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        daerah::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 5; $i++) {
            daerah::create([
                'nama_daerah' => $faker->city,
            ]);
        }
    }
}
