<?php

use Illuminate\Database\Seeder;
Use App\user;

class userSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        user::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 25; $i++) {
            user::create([
                'nama_user' => $faker->name,
                'nomor_hp' => $faker->PhoneNumber,
                'password' => $faker->password,
            ]);
        }
    }
}
