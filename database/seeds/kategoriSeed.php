<?php

use Illuminate\Database\Seeder;
Use App\kategori;

class kategoriSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        kategori::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 5; $i++) {
            kategori::create([
                'nama_kategori' => $faker->word,
            ]);
        }
    }
}
