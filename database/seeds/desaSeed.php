<?php

use Illuminate\Database\Seeder;
Use App\desa;

class desaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // Let's truncate our existing records to start from scratch.
         desa::truncate();

         $faker = \Faker\Factory::create();
 
         // And now, let's create a few articles in our database:
         for ($i = 0; $i < 25; $i++) {
            desa::create([
                 'id_daerah' => $faker->numberBetween($min = 1, $max = 5),
                 'nama_desa' => $faker->streetName,
             ]);
         }
    }
}
