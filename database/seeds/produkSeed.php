<?php

use Illuminate\Database\Seeder;
Use App\produk;

class produkSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        produk::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 25; $i++) {
            produk::create([
                'id_ub' => $faker->numberBetween($min = 1, $max = 5),
                'id_kategori' => $faker->numberBetween($min = 1, $max = 5),
                'nama_produk' => $faker->name,
                'harga_beli' => $faker->numberBetween($min = 10000, $max = 100000),
                'harga_jual' => $faker->numberBetween($min = 10000, $max = 100000),
                'keterangan' => $faker->paragraph,
                'foto' => $faker->imageUrl($width = 600, $height = 600),
            ]);
        }
    }
}
